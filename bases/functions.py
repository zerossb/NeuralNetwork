from matplotlib import pyplot as plt
import numpy as np

from .matrix import Matrix

def name2type(name):
    if name == 'Iris-setosa':
        return 0.0
    elif name == 'Iris-versicolor':
        return 0.5
    elif name == 'Iris-virginica':
        return 1.0

def loadDataset():
    with open("assets/iris.data", "r") as fp_data:
        dataset = []
        for line in fp_data:
            try:
                sepal_length, sepal_width, petal_length, petal_width, name = line.split(
                    ',')
            except ValueError:
                pass
            else:
                dataset.append([
                    sepal_length,
                    sepal_width,
                    petal_length,
                    petal_width,
                    name2type(name.rstrip()),
                ])
    
    matrix = np.array(dataset, dtype="float64")
    result = Matrix(matrix.shape[0], matrix.shape[1])
    result.data = matrix
    return result

def plot_by_result(selected_dataset, neuralNetwork):
    plt.title("Resultado da Análise")

    color = ['ro', 'bo', 'go']
    for i in range(len(selected_dataset)):
        pred = neuralNetwork.prediction(
            selected_dataset[i][0], selected_dataset[i][1])
        if pred < 0.2:
            plt.plot(selected_dataset[i][0],
                     selected_dataset[i][1], color[0])
        elif pred > 0.8:
            plt.plot(selected_dataset[i][0],
                     selected_dataset[i][1], color[1])
        else:
            plt.plot(selected_dataset[i][0],
                     selected_dataset[i][1], color[2])
    plt.show()


def fromList(arr):
    tmp = arr
    if type(tmp) != np.ndarray:
        tmp = np.array(arr)
    else:
        tmp = arr
    result = Matrix(tmp.shape[0], tmp.shape[1])
    result.data = tmp
    return result
