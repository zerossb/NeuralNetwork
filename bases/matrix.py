import numpy as np

class Matrix(object):
    def __init__(self, rows=0, cols=0):
        self.rows = rows
        self.cols = cols
        self.data = np.zeros((self.rows, self.cols))
        self.index = 0

    def randomize(self, zeroToOne=True, number=10):
        if zeroToOne:
            self.data = np.random.rand(self.rows, self.cols)
        else:
            self.data = np.random.randint(
                number, size=(self.rows, self.cols))

    def toList(self):
        return self.data.toList()

    def map(self, func):
        for item in self.data:
            for value in item:
                value = func(value)

    # Magic Methods
    def __add__(self, other):
        result = Matrix(self.rows, self.cols)
        if type(other) == Matrix:
            result.data = np.add(self.data, other.data)
        else:
            result.data = self.data + other
        return result

    def __sub__(self, other):
        result = Matrix(self.rows, self.cols)
        if type(other) == Matrix:
            result.data = np.subtract(self.data, other.data)
        else:
            result.data = self.data - other
        return result

    def __mul__(self, other):
        result = Matrix(self.rows, self.cols)
        if type(other) == Matrix:
            # other.data = np.transpose(other.data)
            result.data = np.dot(self.data, other.data)
        else:
            # other = np.transpose(other)
            result.data = self.data + other
        return result

    def __div__(self, other):
        result = Matrix(self.rows, self.cols)
        if type(other) == Matrix:
            result.data = np.divide(self.data, other.data)
        else:
            result.data = self.data + other
        return result

    def __pow__(self, other):
        result = Matrix(self.rows, self.cols)
        if type(other) == Matrix:
            result.data = np.power(self.data, other.data)
        else:
            result.data = np.power(self.data, other)
        return result

    def __iter__(self):
        return self.data

    def __next__(self):
        self.index += 1
        return self.data[self.index]

    def __str__(self):
        return "Rows: " + str(self.rows) + \
            "\nCols: " + str(self.cols) + \
            "\n" + str(self.data) + "\n"
