from math import exp
import numpy as np

from .matrix import Matrix

import bases.functions as fn


def sigmoid(x):
    return 1 / (1 + exp(-x))


def dsigmoid(y):
    return y * (1 - y)


class NeuralNetwork(object):
    def __init__(self,
                 input_nodes=0,
                 hidden_nodes=0,
                 output_nodes=0):
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = output_nodes

        self.weights_ih = Matrix(self.hidden_nodes, self.input_nodes)
        self.weights_ho = Matrix(self.output_nodes, self.hidden_nodes)
        # Initialize weights
        self.weights_ih.randomize()
        self.weights_ho.randomize()

        self.bias_h = Matrix(self.hidden_nodes, 1)
        self.bias_o = Matrix(self.output_nodes, 1)
        # Initialize Bias
        self.bias_h.randomize()
        self.bias_o.randomize()

        self.learning_rate = 0.1

    def feedforward(self, input_array):
        inputs = fn.fromList(input_array)

        hidden = self.weights_ih * inputs + self.bias_h
        hidden.map(sigmoid)

        output = self.weights_ho * hidden + self.bias_o
        output.map(sigmoid)

        # Arrumo a matriz de saida
        array = []
        for item in output.data:
            array.append([np.sum(item)])

        result = Matrix(output.rows, 1)
        result.data = np.array(array)
        return result

    def train(self, input_array, target_array):
        pass

    def __str__(self):
        return "Input Nodes: " + str(self.input_nodes) + \
            "\nHidden Nodes: " + str(self.hidden_nodes) + \
            "\nOutput Nodes: " + str(self.output_nodes) + "\n"
