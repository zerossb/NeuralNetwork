from bases.nn import NeuralNetwork
from bases.matrix import Matrix

import bases.functions as fn
import numpy as np


def main():
    mtx = fn.loadDataset()
    # mtx = np.delete(mtx, np.s_[-1:], axis=1)

    nn = NeuralNetwork(4, 5, 3)
    for item in mtx.data:
        item = item[:-1]
        item = np.transpose(np.array([item]))
        print(nn.feedforward(item))
    
    # result = nn.feedforward(mtx)
    # print(result)


if __name__ == '__main__':
    main()
